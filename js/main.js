// Маска ввода номера телефона

window.addEventListener("DOMContentLoaded", function() {
    [].forEach.call( document.querySelectorAll('.input-_tel'), function(input) {
    var keyCode;
    function mask(event) {
        event.keyCode && (keyCode = event.keyCode);
        var pos = this.selectionStart;
        if (pos < 3) event.preventDefault();
        var matrix = "+7 ( ___ ) - ___ - ____",
            i = 0,
            def = matrix.replace(/\D/g, ""),
            val = this.value.replace(/\D/g, ""),
            new_value = matrix.replace(/[_\d]/g, function(a) {
                return i < val.length ? val.charAt(i++) || def.charAt(i) : a
            });
        i = new_value.indexOf("_");
        if (i != -1) {
            i < 5 && (i = 3);
            new_value = new_value.slice(0, i)
        }
        var reg = matrix.substr(0, this.value.length).replace(/_+/g,
            function(a) {
                return "\\d{1," + a.length + "}"
            }).replace(/[+()]/g, "\\$&");
        reg = new RegExp("^" + reg + "$");
        if (!reg.test(this.value) || this.value.length < 5 || keyCode > 47 && keyCode < 58) this.value = new_value;
        if (event.type == "blur" && this.value.length < 1)  this.value = "";
    }

    input.addEventListener("input", mask, false);
    input.addEventListener("focus", mask, false);
    input.addEventListener("blur", mask, false);
    input.addEventListener("keydown", mask, false);

});

});

// const telInputs = document.querySelectorAll('.input-_tel');

// telInputs.forEach((input) => {
//     input.addEventListener('input', () => {
//         if (input.value == '+') input.value = '';
//     })
//     input.addEventListener('blur', () => {
//         if (input.value == '+') input.value = '';
//     })
// })

// ------------------------------------------------------------------------------

// Videobox

$('#vidBox').VideoPopUp({
    
    backgroundColor:"#17212a",

    opener:"trigger",

    maxweight:"640",

    idvideo:"example",

    pausevideo:true,

});;

// -----------------------

// map API

    // Функция ymaps.ready() будет вызвана, когда
    // загрузятся все компоненты API, а также когда будет готово DOM-дерево.
    ymaps.ready(init);
    function init(){
        // Создание карты.
        var myMap = new ymaps.Map("map", {
            // Координаты центра карты.
            // Порядок по умолчанию: «широта, долгота».
            // Чтобы не определять координаты центра карты вручную,
            // воспользуйтесь инструментом Определение координат.
            center: [59.942358, 30.339441],
            // Уровень масштабирования. Допустимые значения:
            // от 0 (весь мир) до 19.
            zoom: 16
        });

        var myPlacemark = new ymaps.GeoObject({
            geometry: {
                type: "Point",
                coordinates: [59.942358, 30.339441],
            },
            properties: {
                hintContent: "LoftHause",
                balloonContentHeader: "Санкт-Петербург",
                balloonContentBody: "Набережная реки Фонтанки 10",
                population: 11848762
            }
        });

        myMap.controls.remove('typeSelector');
        myMap.controls.remove('rulerControl');
        myMap.behaviors.disable(['scrollZoom']);
        
        myMap.geoObjects.add(myPlacemark);
    }